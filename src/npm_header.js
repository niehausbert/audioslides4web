/* ---------------------------------------
 Exported Module Variable: AudioSlides4Web
 Package:  audioslides4web
 Version:  2.1.6  Date: 2020/11/30 16:49:23
 Homepage: https://gitlab.com/niehausbert/audioslides4web#readme
 Author:   Engelbert Niehaus
 License:  MIT
 Date:     2020/11/30 16:49:23
 Require Module with:
    const AudioSlides4Web = require('audioslides4web');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */
