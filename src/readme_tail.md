## NPM Library Information
* Exported Module Variable: `AudioSlides4Web`
* Package:  `audioslides4web`
* Version:  `2.1.6`   (last build 2020/11/30 16:49:23)
* Homepage: `https://gitlab.com/niehausbert/audioslides4web#readme`
* License:  MIT
* Date:     2020/11/30 16:49:23
* Require Module with:
```javascript
    const vAudioSlides4Web = require('audioslides4web');
```
* JSHint: installation can be performed with `npm install jshint -g`
